#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2015-2020 Daniel Rodriguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import backtrader as bt
from backtrader.utils.datehelper import *
from backtrader.utils import AutoOrderedDict
from backtrader.utils.datehelper import *
import pandas as pd


__all__ = ['DrawDown', 'TimeDrawDown', 'TimeDrawDownHistory']


class DrawDown(bt.Analyzer):
    '''This analyzer calculates trading system drawdowns stats such as drawdown
    values in %s and in dollars, max drawdown in %s and in dollars, drawdown
    length and drawdown max length

    Params:

      - ``fund`` (default: ``None``)

        If ``None`` the actual mode of the broker (fundmode - True/False) will
        be autodetected to decide if the returns are based on the total net
        asset value or on the fund value. See ``set_fundmode`` in the broker
        documentation

        Set it to ``True`` or ``False`` for a specific behavior

    Methods:

      - ``get_analysis``

        Returns a dictionary (with . notation support and subdctionaries) with
        drawdown stats as values, the following keys/attributes are available:

        - ``drawdown`` - drawdown value in 0.xx %
        - ``moneydown`` - drawdown value in monetary units
        - ``len`` - drawdown length

        - ``max.drawdown`` - max drawdown value in 0.xx %
        - ``max.moneydown`` - max drawdown value in monetary units
        - ``max.len`` - max drawdown length
    '''

    params = (
        ('fund', None),
    )

    def start(self):
        super(DrawDown, self).start()
        if self.p.fund is None:
            self._fundmode = self.strategy.broker.fundmode
        else:
            self._fundmode = self.p.fund

    def create_analysis(self):
        self.rets = AutoOrderedDict()  # dict with . notation

        self.rets.len = 0
        self.rets.drawdown = 0.0
        self.rets.moneydown = 0.0

        self.rets.max.len = 0.0
        self.rets.max.drawdown = 0.0
        self.rets.max.moneydown = 0.0

        self._maxvalue = float('-inf')  # any value will outdo it

    def stop(self):
        self.rets._close()  # . notation cannot create more keys

    # 先调用notify，然后调用next
    def notify_fund(self, cash, value, fundvalue, shares):
        if not self._fundmode:
            self._value = value  # record current value
            self._maxvalue = max(self._maxvalue, value)  # update peak value
        else:
            self._value = fundvalue  # record current value
            self._maxvalue = max(self._maxvalue, fundvalue)  # update peak

    def next(self):
        r = self.rets

        # calculate current drawdown values
        r.moneydown = moneydown = self._maxvalue - self._value
        r.drawdown = drawdown = 100.0 * moneydown / self._maxvalue

        # maxximum drawdown values
        r.max.moneydown = max(r.max.moneydown, moneydown)
        r.max.drawdown = maxdrawdown = max(r.max.drawdown, drawdown)

        r.len = r.len + 1 if drawdown else 0
        r.max.len = max(r.max.len, r.len)


"""
在制定的时间周期内计算回撤
"""


class TimeDrawDown(bt.TimeFrameAnalyzerBase):
    '''This analyzer calculates trading system drawdowns on the chosen
    timeframe which can be different from the one used in the underlying data
    Params:

      - ``timeframe`` (default: ``None``)
        If ``None`` the ``timeframe`` of the 1st data in the system will be
        used

        Pass ``TimeFrame.NoTimeFrame`` to consider the entire dataset with no
        time constraints

      - ``compression`` (default: ``None``)

        Only used for sub-day timeframes to for example work on an hourly
        timeframe by specifying "TimeFrame.Minutes" and 60 as compression

        If ``None`` then the compression of the 1st data of the system will be
        used
      - *None*

      - ``fund`` (default: ``None``)

        If ``None`` the actual mode of the broker (fundmode - True/False) will
        be autodetected to decide if the returns are based on the total net
        asset value or on the fund value. See ``set_fundmode`` in the broker
        documentation

        Set it to ``True`` or ``False`` for a specific behavior

    Methods:

      - ``get_analysis``

        Returns a dictionary (with . notation support and subdctionaries) with
        drawdown stats as values, the following keys/attributes are available:

        - ``drawdown`` - drawdown value in 0.xx %
        - ``maxdrawdown`` - drawdown value in monetary units
        - ``maxdrawdownperiod`` - drawdown length

      - Those are available during runs as attributes
        - ``dd``
        - ``maxdd``
        - ``maxddlen``
    '''

    params = (
        ('fund', None),
    )

    def start(self):
        super(TimeDrawDown, self).start()
        if self.p.fund is None:
            self._fundmode = self.strategy.broker.fundmode
        else:
            self._fundmode = self.p.fund
        self.dd = 0.0               # 当前的回撤
        self.maxdd = 0.0            # 最大回撤
        self.maxddlen = 0           # 最大回撤时长
        self.peak = float('-inf')   # 当前净值创新高峰值
        self.ddlen = 0              # 当前回撤时长

    # 当前周期已经over的时候就计算一次回撤
    def on_dt_over(self):
        if not self._fundmode:
            value = self.strategy.broker.getvalue()
        else:
            value = self.strategy.broker.fundvalue

        # update the maximum seen peak
        if value > self.peak:
            self.peak = value
            self.ddlen = 0  # start of streak

        # calculate the current drawdown
        self.dd = dd = 100.0 * (self.peak - value) / self.peak
        self.ddlen += bool(dd)  # if peak == value -> dd = 0

        # update the maxdrawdown if needed
        self.maxdd = max(self.maxdd, dd)
        self.maxddlen = max(self.maxddlen, self.ddlen)

    def stop(self):
        self.rets['maxdrawdown'] = self.maxdd
        self.rets['maxdrawdownperiod'] = self.maxddlen


"""
指定时间周期的回撤历史
"""


class TimeDrawDownHistory(bt.TimeFrameAnalyzerBase):
    params = (
        ('fund', None),
    )

    def start(self):
        super(TimeDrawDownHistory, self).start()
        if self.p.fund is None:
            self._fundmode = self.strategy.broker.fundmode
        else:
            self._fundmode = self.p.fund

        self.dd_start_date = None   # 回撤开始日期
        self.mdd_low_date = None    # 回撤最低谷的日期
        self.dd_close_date = None   # 回撤结束日期
        self.dd = 0.0               # 当前的回撤
        self.maxdd = 0.0            # 最大回撤
        self.maxddlen = 0           # 最大回撤时长
        self.maxvalue = float('-inf')   # 当前净值创新高峰值
        self.ddlen = 0              # 当前回撤时长

        # 开始的净值进行初始化
        if not self._fundmode:
            self.maxvalue = self.strategy.broker.getvalue()
        else:
            self.maxvalue = self.strategy.broker.fundvalue

        self.rets["date"] = ["start_date", "low_date", "close_date", "max_dd", "max_ddlen"]

    def add_history(self):

        if self.dd_start_date and self.dd_close_date:
            self.rets[self.dd_start_date] = [date2str(self.dd_start_date), date2str(self.mdd_low_date),
                                             date2str(self.dd_close_date), self.maxdd, self.maxddlen]
            self.dd_start_date = None
            self.mdd_low_date = None
            self.dd_close_date = None

    # 当前周期已经over的时候就计算一次回撤
    def on_dt_over(self):
        if not self._fundmode:
            value = self.strategy.broker.getvalue()
        else:
            value = self.strategy.broker.fundvalue

        # update the maximum seen peak
        if value > self.maxvalue:
            if self.dd_close_date is None:
                self.dd_close_date = self.dtkey

            self.add_history()

            self.maxvalue = value
            self.ddlen = 0  # start of streak
            self.dd = 0
        else:
            if self.dd_start_date is None:
                self.dd_start_date = self.dtkey

            if self.mdd_low_date is None:
                self.mdd_low_date = self.dtkey

            # calculate the current drawdown
            self.dd = dd = 100.0 * (self.maxvalue - value) / self.maxvalue
            self.ddlen += (0 if dd == 0 else 1)  # if maxvalue == value -> dd = 0

            # 当前回撤变大了
            if dd > self.maxdd:
                self.mdd_low_date = self.dtkey

            # update the maxdrawdown if needed
            self.maxdd = max(self.maxdd, dd)
            self.maxddlen = max(self.maxddlen, self.ddlen)

    def stop(self):
        self.on_dt_over()
        self.rets['maxdrawdown'] = self.maxdd
        self.rets['maxdrawdownperiod'] = self.maxddlen

    def to_dataframe(self):
        cols = ["date"] + self.rets["date"]
        data = []
        _skip_headers = True
        for key, v in self.rets.items():
            if _skip_headers:
                _skip_headers = False
                continue

            if key in ["maxdrawdown", "maxdrawdownperiod"]:
                continue
            data.append([date2str(key)] + v)

        return pd.DataFrame(columns=cols, data=data)
