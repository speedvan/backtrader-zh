from datetime import datetime


def str2date(date_str, _format_str="%Y-%m-%d %H:%M:%S"):
    return datetime.strptime(date_str, _format_str)


def date2str(date, _format_str="%Y-%m-%d %H:%M:%S"):
    return date.strftime(_format_str)


def date2timestamp(date):
    return date.timestamp()


def str2timestamp(date, _format_str="%Y-%m-%d %H:%M:%S"):
    return date2timestamp(str2date(date, _format_str))


def timestamp2date(timestamp):
    return datetime.fromtimestamp(timestamp)


def timestamp2str(timestamp, _format_str="%Y-%m-%d %H:%M:%S"):
    return date2str(timestamp2date(timestamp), _format_str)
